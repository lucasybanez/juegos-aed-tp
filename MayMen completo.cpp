#include <stdio.h>
#include <cstdlib>
#include <ctime>

main(void)
{
	//Generar el numero secreto en forma aleatorio entre 1 y 999
	srand(time(0));
	int NumSecreto=1+rand()%999;
	
	//Declaramos los indices del metodo de busqueda
	int min=1;
	int max=999;
	
	int num;
	int i=0;
	int Puntaje;
	
	//Utiliamos una bandera para saber si encontro el valor
	bool LoEncontro=false;
	
	//printf("\nEl numero secreto es: %d",NumSecreto);
	
	while(i<10 && LoEncontro==false)
	{
		printf("\nIntento %d: ",i+1);
		scanf("%d",&num);
		
		//Evaluamos el numero ingresado
		
		if(num==NumSecreto)
		{
			LoEncontro=true;
			Puntaje=10-i;	
		}
		else
		{
			if(num<NumSecreto)
			{
				min=num+1;
				printf("\nEl numero secreto es mayor y se encuentra entre %d y %d",min,max);
			}
			else
			{
				max=num-1;
				printf("\nEl numero secreto es menor y se encuentra entre %d y %d",min,max);
			}
		}
	
		i++;
	}
	
	//Indicamos el resultado del juebo
	
	if(LoEncontro==true)
	{
		printf("\nAcertaste! Puntaje obtenido: %d",Puntaje);
	}
	else
	{
		printf("\nFallaste. El numero secreto era %d. Tu puntaje es: 0",NumSecreto);
	}
	
}
