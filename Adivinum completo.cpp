#include <stdio.h>
#include <cstdlib>
#include <ctime>

int GenerarNumSecreto();
bool CompararNumeros(int,int, int &, int &);
bool CompararDigitoNumero(int, int, int &, int &, int);
main ()
{
	int NumSecreto=GenerarNumSecreto();
	int num,mismaposicion,distintaposicion;
	int puntaje=10;
	
	printf("ADIVINUM\n========\nJugaremos con numeros de 4 cifras.");
	printf("\nEl numero secreto es: %d",NumSecreto);
	
	for(int i=1;i<=10;i++)
	{
		printf("\nIntento %d:",i);
		scanf("%d",&num);
			if (num<1000 or num>9999)
			{ printf("El numero ingresado debe tener 4 cifras, vuelva a intentarlo.");
			  i=i-1;
			}
			else 
			{
				//if (num==NumSecreto)
				if(CompararNumeros(num,NumSecreto,distintaposicion,mismaposicion))
				{
					printf("Felicitaciones! Acertaste el numero. Puntaje obtenido: %d",puntaje);
					i=11;
				}
				else 
				{ 
					printf("Cant. Misma Posicion: %d - Cant. Otra Posicion: %d",mismaposicion,distintaposicion);
				    //printf("Cant. Misma Posicion: INSERTAR CONTADOR - Cant. Otra Posicion: INSERTAR CONTADOR");
				    puntaje=puntaje-1;  
				}
			}
	}
	
	printf("\nEl numero secreto era: %d y su puntaje es: %d \n", NumSecreto, puntaje);
	system("pause");
}

int GenerarNumSecreto()
{
	int DigUno, DigDos, DigTres, DigCuatro,i;             
	
	srand(time(0));
	
	 DigUno=1+rand()%9; //el primer digito no puede ser cero
	do
	{		
	        DigDos=rand()%9;
		
	}while(DigUno==DigDos);
	do
	{	
	        DigTres=rand()%9;
		
	}while(DigUno==DigTres or DigDos==DigTres);
	do
	{	
	        DigCuatro=rand()%9;
		
	}while(DigUno==DigCuatro or DigDos==DigCuatro or DigTres==DigCuatro);
	
	int NumSecreto=DigUno*1000+DigDos*100+DigTres*10+DigCuatro;	
}

bool CompararNumeros(int num,int NumSecreto, int &distintaposicion, int & mismaposicion)
{
	distintaposicion=0;
	mismaposicion=0;
	bool dig1, dig2, dig3, dig4;
	
	dig1=CompararDigitoNumero(num/1000,NumSecreto, distintaposicion, mismaposicion, 1);
	dig2=CompararDigitoNumero(((NumSecreto%100)/10),NumSecreto, distintaposicion, mismaposicion, 2);
	dig3=CompararDigitoNumero((NumSecreto%1000)/100,NumSecreto, distintaposicion, mismaposicion, 3);
	dig4=CompararDigitoNumero(num%10,NumSecreto, distintaposicion, mismaposicion, 4);
	
	if(dig1 && dig2 && dig3 && dig4)
	{
		return true;
	}
	else
	{
		return false;
	}
	
	
}

bool CompararDigitoNumero(int digito, int NumSecreto, int &distintaposicion, int & mismaposicion, int posicion)
{
	bool EsIgual=false;
	if(digito==NumSecreto/1000) //unidad de mil
	{
		if(posicion==1)
		{
			mismaposicion++;
			EsIgual=true;
		}
		else
		{
			distintaposicion++;
			//printf("\nDistinta posicion 1: %d",distintaposicion);
		}
	}
	if(digito==(NumSecreto%1000)/100) // centena
	{
		if(posicion==2)
		{
			mismaposicion++;
			EsIgual=true;
		}
		else
		{
			distintaposicion++;
		}
	}
	//printf("\nSegunda posicion: %d",((NumSecreto%100)/10));
	if(digito==(NumSecreto%100)/10) // decena
	{
		if(posicion==3)
		{
			mismaposicion++;
			EsIgual=true;
		}
		else
		{
			distintaposicion++;
		}
	}
	//printf("\nTercera posicion: %d",((NumSecreto%1000)/100));
	if(digito==NumSecreto%10) //unidad
	{
		if(posicion==4)
		{
			mismaposicion++;
			EsIgual=true;
		}
		else
		{
			distintaposicion++;
		}
	}
	return EsIgual;
}
