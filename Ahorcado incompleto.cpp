#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <ctime>

	void PalabraSecreta (char []);
	int LongitudPalabra (char []);
	int Comparar(char,char [],bool []);
	void EscribirPalabraAdivinar(char [],bool []);
	void EscribirEstado(int);
	void ValidadLetra(char &Letra);   
	bool VerificarIngresoLetra(char , char []); 
	void InicializarLetrasIngresadas(char []);
	void ComprobarLetra(char &, char []);
	//void imprimirarreglo(char []);
main()
{
	//variables
	char Palabra[11];
	bool Adivinada[11];
	int Jugada=0;
	char Letra;
	int Contador=0;
	int Acumulador=0;
	int Errada=0;
	char Caracter;
	char LetrasIngresadas[26];
	
	//incializar adivinada
	for(int i=0; i<11;i++)
	{
		Adivinada[i]=false;
	}
	
	InicializarLetrasIngresadas(LetrasIngresadas);
	printf ("AHORCADO\n=======\nINICIO\n");
	printf ("Palabras a adivinar: ");
	PalabraSecreta(Palabra);
	
	//Escribimos los guines de la palabra
	for(int i=0;i<LongitudPalabra(Palabra);i++)
	{
		printf("_ ");
	}
	printf("\n");
	
	//MOSTRAR PALABRA SECRETA
	/*printf("\nPalabra secreta: ");
	for(int i=0;i<LongitudPalabra(Palabra);i++)
	{
		printf("%c",Palabra[i]);
	}
	printf("\n");*/
	
	while(Acumulador<LongitudPalabra(Palabra) && Errada<10)
	{
		Jugada++;
		printf("\nJUGADA #%d",Jugada);
		printf("\nIngrese la letra: ");
		scanf("%c",&Letra);
		scanf("%c",&Caracter);
		ValidadLetra(Letra);
		ComprobarLetra(Letra,LetrasIngresadas);
		printf("\nRESULTADO JUGADA #%d",Jugada);
		Contador=Comparar(Letra,Palabra,Adivinada);
		if(Contador<1)
		{
			Errada++;
		}
		EscribirPalabraAdivinar(Palabra,Adivinada);
		Acumulador=Acumulador+Contador;
		EscribirEstado(Errada);
		
	}
	
	if(Errada<10)
	{
		printf("\nHAS GANADO LA PARTIDA!!!");
		printf("\nEl puntaje obtenido es de: %d PUNTOS.",50-(2*Errada));
	}
	else
	{
		printf("\nHAS PERDIDO LA PARTIDA!!!");
		printf("\nEl puntaje obtenido es de: 0 PUNTOS.");
	}
	
	
	
	
	
	
	
	
}
	/*void imprimirarreglo(char arreglo[26])
	{
		printf("\nArreglo: ");
		for(int i=0;i<26;i++)
		{
			printf("%c",arreglo[i]);
		}
	}*/
	void ComprobarLetra(char &Letra, char LetrasIngresadas[26])
	{
		char caracter;
		while(VerificarIngresoLetra(Letra,LetrasIngresadas)==true)
		{
			printf("\n La letra %c ya fue ingresada, debe ingresar otra distinta: ");
			scanf("%c",&Letra);
			scanf("%c",&caracter);
		}
	}
	void InicializarLetrasIngresadas(char LetrasIngresadas[26])
	{
		for(int i=0;i<26;i++)
		{
			LetrasIngresadas[i]='-';
		}
	}

	bool VerificarIngresoLetra(char Letra, char LetrasIngresadas[26]) //si no esta ingresada retorna false, si esta ingresada retorna true
	{
		bool Ingresada=false;
		int i=0;
		do
		{
			if(LetrasIngresadas[i]==Letra)
			{
				Ingresada=true;
			}
			i++;
		}while(LetrasIngresadas[i-1]!='-' && Ingresada==false);
		
		//imprimirarreglo(LetrasIngresadas);
		
		if(Ingresada==false)
		{
			LetrasIngresadas[i-1]=Letra;
		}
		return Ingresada;
	}

	void ValidadLetra(char &Letra)
	{
		char caracter;
		while(Letra<'A' || Letra>'z' || (Letra>'Z' && Letra<'a'))
		{
			printf("\nDebe ingresar una letra: ");
			scanf("%c",&Letra);
			scanf("%c",&caracter);
		}
		if(Letra>='a' && Letra<='z')
		{
			Letra=Letra-32; //convertimos la letra minuscula en mayuscula con codigo ascii
		}
	}
	void EscribirEstado(int Errada)
	{
		switch (Errada)
		{
			case 1:
				printf("\nEstado del mu�eco: cabeza");
			break;
			case 2:
				printf("\nEstado del mu�eco: cabeza - tronco");
			break;
			case 3:
				printf("\nEstado del mu�eco: cabeza - tronco - brazo izquierdo");
			break;
			case 4:
				printf("\nEstado del mu�eco: cabeza - tronco- brazo izquierdo - brazo derecho");
			break;
			case 5:
				printf("\nEstado del mu�eco: cabeza - tronco - brazo izquierdo - brazo derecho - pierna izquierda");
			break;
			case 6:
				printf("\nEstado del mu�eco: cabeza - tronco- brazo izquierdo - brazo derecho - pierna izquierda - pierna derecha");
			break;
			case 7:
				printf("\nEstado del mu�eco: cabeza - tronco- brazo izquierdo - brazo derecho - pierna izquierda - pierna derecha - mano izquierda");
			break;
			case 8:
				printf("\nEstado del mu�eco: cabeza - tronco- brazo izquierdo - brazo derecho - pierna izquierda - pierna derecha - mano izquierda - mano derecha");
			break;
			case 9:
				printf("nEstado del mu�eco: cabeza - tronco- brazo izquierdo - brazo derecho - pierna izquierda - pierna derecha - mano izquierda - mano derecha - pie izquierdo");
			break;
			case 10:
				printf("\nEstado del mu�eco: cabeza - tronco- brazo izquierdo - brazo derecho - pierna izquierda - pierna derecha - mano izquierda - mano derecha - pie izquierdo - pie derecho");
			break;
		}
	}
	
	int Comparar(char Letra,char Palabra[],bool Adivinada[])
	{
		int contador=0;
		for(int i=0;i<11;i++)
		{
			if(Palabra[i]==Letra)
			{
				Adivinada[i]=true;
				contador++;
			}
		}
		return contador;
	}
	
	void EscribirPalabraAdivinar(char Palabra[],bool Adivinada[])
	{
			printf ("\nPalabras a adivinar: ");
			for(int i=0;i<LongitudPalabra(Palabra);i++)
			{
				if(Adivinada[i]==true)
				{
					printf("%c ",Palabra[i]);
				}
				else
				{
					printf("_ ");
				}
				
			}
			printf("\n");
	}

	void PalabraSecreta (char Palabra[11])
	{
		srand(time(0));
	 	int num=1+rand()%(30-1);
	 	switch (num)
	 	{
	 		case 1: 
			 Palabra[0]='C';
			 Palabra[1]='O';
			 Palabra[2]='M';
			 Palabra[3]='P';
			 Palabra[4]='I';
			 Palabra[5]='L';
			 Palabra[6]='A';
			 Palabra[7]='D';
			 Palabra[8]='O';
			 Palabra[9]='R';
			 Palabra[10]='-';
			 //Palabra={'C','o','m','p','i','l','a','d','o','r','-'};
	 			break;
	 		case 2:
			 Palabra[0]='E';
			 Palabra[1]='N';
			 Palabra[2]='T';
			 Palabra[3]='O';
			 Palabra[4]='R';
			 Palabra[5]='N';
			 Palabra[6]='O';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
			 //Palabra={'E','n','t','o','r','n','o','-','-','-','-'};
	 			break;
	 		case 3:
			 Palabra[0]='C';
			 Palabra[1]='O';
			 Palabra[2]='M';
			 Palabra[3]='P';
			 Palabra[4]='U';
			 Palabra[5]='T';
			 Palabra[6]='A';
			 Palabra[7]='D';
			 Palabra[8]='O';
			 Palabra[9]='R';
			 Palabra[10]='A'; 
			 //Palabra={'C','o','m','p','u','t','a','d','o','r','a'};
	 			break;
	 			case 4:
			 Palabra[0]='E';
			 Palabra[1]='S';
			 Palabra[2]='T';
			 Palabra[3]='R';
			 Palabra[4]='U';
			 Palabra[5]='C';
			 Palabra[6]='T';
			 Palabra[7]='U';
			 Palabra[8]='R';
			 Palabra[9]='A';
			 Palabra[10]='-'; 
	 			break;
	 			case 5:
			 Palabra[0]='S';
			 Palabra[1]='E';
			 Palabra[2]='L';
			 Palabra[3]='E';
			 Palabra[4]='C';
			 Palabra[5]='C';
			 Palabra[6]='I';
			 Palabra[7]='O';
			 Palabra[8]='N';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 6:
			 Palabra[0]='R';
			 Palabra[1]='E';
			 Palabra[2]='P';
			 Palabra[3]='E';
			 Palabra[4]='T';
			 Palabra[5]='I';
			 Palabra[6]='C';
			 Palabra[7]='I';
			 Palabra[8]='O';
			 Palabra[9]='N';
			 Palabra[10]='-'; 
	 			break;
	 			case 7:
			 Palabra[0]='D';
			 Palabra[1]='I';
			 Palabra[2]='R';
			 Palabra[3]='E';
			 Palabra[4]='C';
			 Palabra[5]='T';
			 Palabra[6]='I';
			 Palabra[7]='V';
			 Palabra[8]='A';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 8:
			 Palabra[0]='A';
			 Palabra[1]='L';
			 Palabra[2]='G';
			 Palabra[3]='O';
			 Palabra[4]='R';
			 Palabra[5]='I';
			 Palabra[6]='T';
			 Palabra[7]='M';
			 Palabra[8]='O';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 9:
			 Palabra[0]='P';
			 Palabra[1]='R';
			 Palabra[2]='O';
			 Palabra[3]='G';
			 Palabra[4]='R';
			 Palabra[5]='A';
			 Palabra[6]='M';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 10:
			 Palabra[0]='E';
			 Palabra[1]='J';
			 Palabra[2]='E';
			 Palabra[3]='C';
			 Palabra[4]='U';
			 Palabra[5]='C';
			 Palabra[6]='I';
			 Palabra[7]='O';
			 Palabra[8]='N';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 11:
			 Palabra[0]='I';
			 Palabra[1]='N';
			 Palabra[2]='T';
			 Palabra[3]='-';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 12:
			 Palabra[0]='F';
			 Palabra[1]='L';
			 Palabra[2]='O';
			 Palabra[3]='A';
			 Palabra[4]='T';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 13:
			 Palabra[0]='C';
			 Palabra[1]='H';
			 Palabra[2]='A';
			 Palabra[3]='R';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 14:
			 Palabra[0]='D';
			 Palabra[1]='O';
			 Palabra[2]='U';
			 Palabra[3]='B';
			 Palabra[4]='L';
			 Palabra[5]='E';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 15:
			 Palabra[0]='L';
			 Palabra[1]='O';
			 Palabra[2]='N';
			 Palabra[3]='G';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 16:
			 Palabra[0]='I';
			 Palabra[1]='F';
			 Palabra[2]='-';
			 Palabra[3]='-';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 17:
			 Palabra[0]='E';
			 Palabra[1]='L';
			 Palabra[2]='S';
			 Palabra[3]='E';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 18:
			 Palabra[0]='F';
			 Palabra[1]='O';
			 Palabra[2]='R';
			 Palabra[3]='-';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 19:
			 Palabra[0]='W';
			 Palabra[1]='H';
			 Palabra[2]='I';
			 Palabra[3]='L';
			 Palabra[4]='E';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 20:
			 Palabra[0]='R';
			 Palabra[1]='E';
			 Palabra[2]='T';
			 Palabra[3]='U';
			 Palabra[4]='R';
			 Palabra[5]='N';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 21:
			 Palabra[0]='B';
			 Palabra[1]='R';
			 Palabra[2]='E';
			 Palabra[3]='A';
			 Palabra[4]='K';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 22:
			 Palabra[0]='S';
			 Palabra[1]='W';
			 Palabra[2]='I';
			 Palabra[3]='T';
			 Palabra[4]='C';
			 Palabra[5]='H';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 23:
			 Palabra[0]='C';
			 Palabra[1]='A';
			 Palabra[2]='S';
			 Palabra[3]='E';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 24:
			 Palabra[0]='M';
			 Palabra[1]='A';
			 Palabra[2]='I';
			 Palabra[3]='N';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 25:
			 Palabra[0]='I';
			 Palabra[1]='N';
			 Palabra[2]='C';
			 Palabra[3]='L';
			 Palabra[4]='U';
			 Palabra[5]='D';
			 Palabra[6]='E';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 26:
			 Palabra[0]='D';
			 Palabra[1]='E';
			 Palabra[2]='F';
			 Palabra[3]='I';
			 Palabra[4]='N';
			 Palabra[5]='E';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 		case 27:
			 Palabra[0]='S';
			 Palabra[1]='T';
			 Palabra[2]='R';
			 Palabra[3]='U';
			 Palabra[4]='C';
			 Palabra[5]='T';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 28:
			 Palabra[0]='C';
			 Palabra[1]='I';
			 Palabra[2]='N';
			 Palabra[3]='-';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 29:
			 Palabra[0]='C';
			 Palabra[1]='O';
			 Palabra[2]='U';
			 Palabra[3]='T';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			case 30:
			 Palabra[0]='E';
			 Palabra[1]='N';
			 Palabra[2]='D';
			 Palabra[3]='L';
			 Palabra[4]='-';
			 Palabra[5]='-';
			 Palabra[6]='-';
			 Palabra[7]='-';
			 Palabra[8]='-';
			 Palabra[9]='-';
			 Palabra[10]='-'; 
	 			break;
	 			
	 	}
	 	
	 	
	 	
	 	
	 	
	 	
	}
	int LongitudPalabra (char Palabra [11])
	{
		int num=0;
		
		for(int i=0;i<11;i++)
		{
			if(Palabra[i]!='-')
			{
				num++;
			}
		}
		return num;
	}
